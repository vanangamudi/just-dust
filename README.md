# just-dust

Dust-like particles acting under the gravitational pull of each other. Just dust in free space ;)

It's a remake of the [particle game](https://gitlab.com/chrismit3s/particle-game) I made some time ago. This time with a canvas wrapper and export as video functionality.